const placeholderMap = {
  dimensions: [40, 20],
  walls: {
    "16x6": 1,
    "17x6": 1,
    "16x7": 1,
    "17x7": 1,
    "21x3": 1,
    "22x3": 1,
    "23x3": 1,
    "21x4": 1,
    "22x4": 1,
    "23x4": 1
  },
  spawnPlaces: [[10, 7]]
};

module.exports = placeholderMap;
