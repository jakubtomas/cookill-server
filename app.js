require("dotenv").config();
const io = require("socket.io")();

const placeholderMap = require("./maps/placeholder");

let rooms = {};

io.on("connection", socket => {
  console.log(socket.id);

  socket.on("joinGame", ({ nickname }) => {
    const newPlayer = {
      id: socket.id,
      nickname: nickname,
      room: 0,
      playerPosition: [0, 0],
      bullets: []
    };

    if (Object.keys(rooms).length > 0) {
      const roomsKeys = Object.keys(rooms);

      rooms = {
        ...rooms,
        [roomsKeys.length - 1]: {
          players: [...rooms[roomsKeys.length - 1].players, newPlayer],
          map: placeholderMap.walls,
          mapDimension: placeholderMap.dimensions,
          spawnPlace: placeholderMap.spawnPlaces[0]
        }
      };
    } else {
      rooms = {
        ...rooms,
        [0]: {
          players: [newPlayer],
          map: placeholderMap.walls,
          mapDimension: placeholderMap.dimensions,
          spawnPlace: placeholderMap.spawnPlaces[0]
        }
      };
    }

    socket.emit("joinedGame", {
      player: newPlayer,
      room: rooms[newPlayer.room]
    });
  });

  socket.on("sendPlayerData", data => {
    const playerId = Object.keys(data)[0];
    const { playerPosition, bullets, room } = data[playerId];

    if (rooms[room]) {
      const player = rooms[room].players.find(player => player.id === playerId);

      player.playerPosition = playerPosition;
      player.bullets = bullets;

      io.emit("recieveGameData", rooms[room].players);
    }
  });
});

io.listen(process.env.PORT);
